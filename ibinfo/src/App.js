import About from './components/About';
import Hero from './components/Hero';
import Navbar from './components/Navbar';
import Contact from './components/Contact'; 
import AllInOne from './components/AllInOne';


function App() {
  return (
    <>
    <Navbar/>
    <Hero/>
    <About/>
    <Contact/>
    <AllInOne/>
    </>
  );
}

export default App;
